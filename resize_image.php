<?php
/**
 * Resize Image version 1.0
 * @author Radoslaw Kurowski
 * @email  radix@salvilines.eu
 */
class ResizeImage
{
    const LOG_TYPE_INFO    = 'info';
    const LOG_TYPE_SPECIAL = 'special';
    const LOG_TYPE_ERROR   = 'error';

    const OPTIONS_SHORT = 'i:o:w:h:x';
    const OPTIONS_LONG  = [
        'i' => 'input:',
        'o' => 'output:',
        'w' => 'width:',
        'h' => 'height:',
        'x' => 'help'
    ];

    const CREDS = "Image resize script v1.0. Author: Radoslaw Kurowski, radix@salvilines.eu\nFor help type php resize-image.php -x";
    const HELP  = "Script usage: php resize-image.php -i input.jpg -o output.jpg -h 1000 -w 800\n\nOptions:\n-i --input  | the input path of the image\n-o --output | the output path of the converted image\n-w --width  | the desired output width\n-h --height | the desired input height\n-x --help   | prints this screen\n";

    private $inputPath;
    private $outputPath;
    private $targetWidth  = null;
    private $targetHeight = null;


    /**
     * Validate options and initialize values.
     */
    public function __construct()
    {
        $this->log(self::CREDS, self::LOG_TYPE_SPECIAL);
        $options = getopt(self::OPTIONS_SHORT, self::OPTIONS_LONG);

        //help options present, display help screen and terminate
        if (isset($options['help']) || isset($options['x'])) {
            $this->log(self::HELP);
        } else {
            //if no help screen continue normal processing
            $this->log('Initializing...');

            if ($this->validateOptions($options) !== true) {
                exit();
            } else {
                $this->setInput(!empty($options['i']) ? $options['i'] : $options['input']);
                $this->setOutput(!empty($options['o']) ? $options['o'] : $options['output']);
                $this->setWidth((int)!empty($options['w']) ? $options['w'] : (!empty($options['width']) ? $options['width'] : null));
                $this->setHeight((int)!empty($options['h']) ? $options['h'] : (!empty($options['height']) ? $options['height'] : null));
            }
        }
    }


    /**
     * Display end message.
     */
    public function __destruct()
    {
        $this->log('Good bye!');
    }


    /**
     * Resize the image.
     */
    public function run()
    {
        $this->log('Resizing...');
        list($originalWidth, $originalHeight, $imageType) = getimagesize($this->getInput());

        if (empty($originalWidth) || empty($originalHeight)) {
            $this->log('Source image has incorrect size.', self::LOG_TYPE_ERROR);
            exit();
        }

        $oldImage = null;

        //create an image handler
        switch ($imageType) {
            case IMAGETYPE_GIF:
                $oldImage = imagecreatefromgif($this->getInput());
                break;
            case IMAGETYPE_JPEG:
                $oldImage = imagecreatefromjpeg($this->getInput());
                break;
            case IMAGETYPE_PNG:
                $oldImage = imagecreatefrompng($this->getInput());
                break;
            default:
                $this->log('Unknown image type.', self::LOG_TYPE_ERROR);
                exit();
        }

        list($targetWidth, $targetHeight) =
            $this->calculateTargetSize($originalWidth, $originalHeight, $this->getWidth(), $this->getHeight());

        //create new image handler
        $newImage = imagecreatetruecolor($targetWidth, $targetHeight);

        //copy the old image into a new image
        imagecopyresampled(
            $newImage,
            $oldImage,
            0,
            0,
            0,
            0,
            $targetWidth,
            $targetHeight,
            $originalWidth,
            $originalHeight
        );

        //save new image to provided path
        switch ($imageType) {
            case IMAGETYPE_GIF:
                imagegif($newImage, $this->getOutput());
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($newImage, $this->getOutput(), 85);
                break;
            case IMAGETYPE_PNG:
                imagepng($newImage, $this->getOutput(), 6);
                break;
        }

        imagedestroy($newImage);
        $this->log('Done.');
    }


    /**
     * Inpot setter.
     *
     * @param string $input
     */
    private function setInput($input)
    {
        $this->inputPath = $input;
    }


    /**
     * Output setter
     *
     * @param string $output
     */
    private function setOutput($output)
    {
        $this->outputPath = $output;
    }


    /**
     * Width setter.
     *
     * @param int $width
     */
    private function setWidth($width)
    {
        $this->targetWidth = $width;
    }


    /**
     * Height setter.
     *
     * @param int $height
     */
    private function setHeight($height)
    {
        $this->targetHeight = $height;
    }


    /**
     * Input getter.
     *
     * @return string
     */
    private function getInput()
    {
        return $this->inputPath;
    }


    /**
     * Output getter.
     *
     * @return string
     */
    private function getOutput()
    {
        return $this->outputPath;
    }


    /**
     * Width getter.
     *
     * @return int
     */
    private function getWidth()
    {
        return $this->targetWidth;
    }


    /**
     * Height getter.
     *
     * @return int
     */
    private function getHeight()
    {
        return $this->targetHeight;
    }


    /**
     * Validate script options.
     *
     * @param array $options
     *
     * @return bool
     */
    private function validateOptions(array $options)
    {
        $errors = [];

        //check values
        $input  = !empty($options['i']) ? $options['i'] : (!empty($options['input']) ? $options['input'] : null);
        $output = !empty($options['o']) ? $options['o'] : (!empty($options['output']) ? $options['output'] : null);
        $width  = !empty($options['w']) ? $options['w'] : (!empty($options['width']) ? $options['width'] : null);
        $height = !empty($options['h']) ? $options['h'] : (!empty($options['height']) ? $options['height'] : null);

        if (is_null($input)) {
            $errors[] = 'no input path specified';
        }

        if (is_null($output)) {
            $errors[] = 'no output path specified';
        }

        if (is_null($width) && is_null($height)) {
            $errors[] = 'either target height or width must be provided';
        } else if ((!is_null($width) && !ctype_digit($width)) || (!is_null($height) && !ctype_digit($height))) {
            $errors[] = 'height and width must be full numbers';
        }

        //check permissions and paths
        if (!file_exists($input)) {
            $errors[] = 'invalid input path';
        } else {
            if (!is_readable($input)) {
                $errors[] = 'could not read input path - check permissions';
            }
        }

        if (file_exists($output)) {
            $errors[] = 'output file already exists';
        }

        //check file extensions
        if (count($errors) === 0) {
            $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
            if (strstr(finfo_file($fileInfo, $input), 'image') === false) {
                $errors[] = 'input file is not an image';
            }
            finfo_close($fileInfo);

            //input and output file extensions must match
            $inputFileParts  = pathinfo($input);
            $outputFileParts = pathinfo($output);

            if ($inputFileParts['extension'] !== $outputFileParts['extension']) {
                $errors[] = 'input file and output file extensions must be the same';
            }

        }

        //display errors
        if (count($errors) > 0) {
            $this->log('Invalid options: '.implode(', ', $errors), self::LOG_TYPE_ERROR);
            return false;
        } else {
            return true;
        }
    }


    /**
     * Get the target height and width. If only one target is provided calculate the other.
     *
     * @param int $sourceWidth
     * @param int $sourceHeight
     * @param int $targetWidth  Optional. If not provided the $targetHeight must be provided.
     * @param int $targetHeight Optional. If not provided the $targetWidth must be provided.
     *
     * @return array
     */
    private function calculateTargetSize($sourceWidth, $sourceHeight, $targetWidth = null, $targetHeight = null)
    {
        if (is_null($targetWidth)) {
            $targetWidth = $targetHeight * $sourceWidth / $sourceHeight;
        }

        if (is_null($targetHeight)) {
            $targetHeight = $targetWidth * $sourceHeight / $sourceWidth;
        }

        return [$targetWidth, $targetHeight];
    }


    /**
     * Log a message.
     *
     * @param string $message The message to log.
     * @param string $type    Type of message.
     */
    private function log($message, $type = self::LOG_TYPE_INFO)
    {
        if ($type === self::LOG_TYPE_ERROR) {
            $message = "\033[0;31m" . $message . "\033[m\n"; //make string red
        } else if ($type === self::LOG_TYPE_SPECIAL) {
            $message = "\033[1;34m" . $message . "\033[m\n"; //make string blue
        }
        echo $message."\n";
    }
}

//execute script
$ri = new ResizeImage();
$ri->run();